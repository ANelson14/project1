package com.util;

public class CONSTANTVALUES {
	
	public static final int USEREMPLOYEE = 1;
	public static final String EMPLOYEE = "Employee";
	public static final int USERMANAGER = 2;
	public static final String MANAGER = "Manager";
	public static final int TYPECERTIF = 1;
	public static final String CERTIFICATE = "Cerification";
	public static final int TYPETRAVEL = 2;
	public static final String TRAVEL = "Travel";
	public static final int TYPELODGINGS = 3;
	public static final String LODGINGS = "Lodgings";
	public static final int TYPEMEALS = 4;
	public static final String MEALS = "Meals";
	public static final int TYPEOTHER = 5;
	public static final String OTHER = "Other";
	public static final int STATUSPEND = 1;
	public static final String PENDING = "Pending";
	public static final int STATUSAPPROV = 2;
	public static final String APPROVED = "Approved";
	public static final int STATUSDENIED = 3;
	public static final String DENIED = "Denied";
	

}
