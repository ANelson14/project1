package com.example.pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class UserLoginPage {

	private WebDriver driver;
	private WebElement username;
	private WebElement password;
	private WebElement loginSubmit;
	private WebElement reimbamount;
	private WebElement description;
	private Select selectNumbs;
	private WebElement domButton;
	private WebElement logout;
	private WebElement approveButton;
	public UserLoginPage() {
		// TODO Auto-generated constructor stub
	}
	public UserLoginPage(WebDriver driver)
	{
		this.driver = driver;
		this.navigateTo();
		this.username = driver.findElement(By.name("username"));
		this.password = driver.findElement(By.name("password"));
		this.loginSubmit = driver.findElement(By.name("loginsubmit"));
		
	}
	public String getUsername() {
		return this.username.getAttribute("value");
	}
	public void setUsername(String usernane) {
		this.username.clear();
		this.username.sendKeys(usernane);
	}
	public String getPassword() {
		return this.password.getAttribute("value");
	}
	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}
	
	public String getReimbamount() {
		return this.reimbamount.getAttribute("value");
	}
	public void setReimbamount(String reimbamount) {
		this.reimbamount = driver.findElement(By.id("reimbamount"));

		this.reimbamount.clear();
		this.reimbamount.sendKeys(reimbamount);
	}
	public String getDescription() {
		return this.description.getAttribute("value");
	}
	public void setDescription(String description) {
		this.description = driver.findElement(By.id("description1"));

		this.description.clear();
		this.description.sendKeys(description);
	}

	public void setSelectNumbs(String selectNumbs) {
		this.selectNumbs  = new Select(driver.findElement(By.id("selectnum")));
		System.out.println(selectNumbs);
		//this.selectNumbs.clear();
		this.selectNumbs.selectByValue(selectNumbs);;
	}
	public String getLogout() {
		return this.logout.getAttribute("value");
	}
	public void setLogout(String logout) {
		this.logout = driver.findElement(By.id("logout"));
		this.loginSubmit.clear();
		this.logout.sendKeys(logout);
	}
	public void submitLogin() {
		this.loginSubmit.click();
	}
	public void submitReimb() {
		this.domButton = driver.findElement(By.id("submitreimb"));
		this.domButton.click();
	}
	public void approveReimb(String xpath) {
		this.approveButton = driver.findElement(By.xpath(xpath));
		System.out.println(this.approveButton.getAttribute("id"));
		this.approveButton.click();
	}
	public void navigateTo() {
		this.driver.get("http://localhost:9011/html/index.html");
	}
	public void navigateTo2(String url) {
		this.driver.get("http://localhost:9011/html/submitmanReimbursement.html");
	}
}
