package com.project1;

import org.apache.log4j.Logger;

import com.project1.controllers.UserController;
import com.project1.dao.Project1DaoConnection;
import com.project1.dao.ReimbursementDao;
import com.project1.dao.UserDao;
import com.project1.exceptions.FailedUpdateException;
import com.project1.exceptions.NoUserException;
import com.project1.service.ReimbursementService;
import com.project1.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	public final static Logger log = Logger.getLogger(MainDriver.class);

	public static void main(String[] args) {
		UserController uCon = new UserController(new UserService(new UserDao(new Project1DaoConnection())),new ReimbursementService(new ReimbursementDao(new Project1DaoConnection())));
		
		Javalin app = Javalin.create(config->{
			config.addStaticFiles("/frontend");
		});
		
		
		
		
		
		
		app.start(9011);
		
		app.post("/project1/login", uCon.postLogin);
		app.get("/project1/reimbmansess", uCon.getreimbManSession);
		app.get("/project1/session", uCon.getSessionUser);
		app.get("project1/reimbempsess",uCon.getEmployeeReim);
		app.put("/project1/update",uCon.updateReimb);
		app.post("/project1/addreimb", uCon.addReimb);
		app.get("/project1/logout", uCon.getlogout);
		app.exception(NoUserException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("http://localhost:9011/html/index.html");
			ctx.result("user does not exist");
		}).exception(FailedUpdateException.class, (e, ctx)->{
			ctx.status(404);
			ctx.result("failed to update");
			
		});
	}
}
