package com.project1.classmodels;

import java.sql.Timestamp;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Reimbursement {

	private int reimbId;
	private int reimbAmount;
	@JsonFormat(pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
	private java.sql.Timestamp reimbSubmitted;
	@JsonFormat(pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
	private java.sql.Timestamp reimbResolved;
	private String reimbDescription;
	private  byte[] reimbReceipt;
	private int reimbAuthor;
	private int reimbResolver;
	private int reimbStatusId;
	private int reimbTypeId;
	private String reimbType;
	private String reimbStatus;
	private String reimbAuthorUser;
	private String reimbResolverUser;
	
	
public Reimbursement() {
	// TODO Auto-generated constructor stub
}


public Reimbursement(int reimbAmount, String reimbDescription, int reimbAuthor, int reimbTypeId) {
	super();
	this.reimbAmount = reimbAmount;
	this.reimbDescription = reimbDescription;
	this.reimbAuthor = reimbAuthor;
	this.reimbTypeId = reimbTypeId;
}


public Reimbursement(int reimbId, int reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved,
		String reimbDescription,  String reimbAuthorUser,
		String reimbResolverUser, String reimbStatus,String reimbType) {
	super();
	this.reimbId = reimbId;
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbResolved = reimbResolved;
	this.reimbDescription = reimbDescription;
	this.reimbType = reimbType;
	this.reimbStatus = reimbStatus;
	this.reimbAuthorUser = reimbAuthorUser;
	this.reimbResolverUser = reimbResolverUser;
}


public String getReimbType() {
	return reimbType;
}


public void setReimbType(String reimbType) {
	this.reimbType = reimbType;
}


public String getReimbStatus() {
	return reimbStatus;
}


public void setReimbStatus(String reimbStatus) {
	this.reimbStatus = reimbStatus;
}


public String getReimbAuthorUser() {
	return reimbAuthorUser;
}


public void setReimbAuthorUser(String reimbAuthorUser) {
	this.reimbAuthorUser = reimbAuthorUser;
}


public String getReimbResolverUser() {
	return reimbResolverUser;
}


public void setReimbResolverUser(String reimbResolverUser) {
	this.reimbResolverUser = reimbResolverUser;
}


public Reimbursement(int reimbId, int reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved,
		String reimbDescription, int reimbAuthor, int reimbResolver, int reimbStatusId, int reimbTypeId) {
	super();
	this.reimbId = reimbId;
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbResolved = reimbResolved;
	this.reimbDescription = reimbDescription;
	this.reimbAuthor = reimbAuthor;
	this.reimbResolver = reimbResolver;
	this.reimbStatusId = reimbStatusId;
	this.reimbTypeId = reimbTypeId;
}


public Reimbursement(int reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription,
		byte[] reimbReceipt, int reimbAuthor, int reimbResolver, int reimbStatusId, int reimbTypeId) {
	super();
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbResolved = reimbResolved;
	this.reimbDescription = reimbDescription;
	this.reimbReceipt = reimbReceipt;
	this.reimbAuthor = reimbAuthor;
	this.reimbResolver = reimbResolver;
	this.reimbStatusId = reimbStatusId;
	this.reimbTypeId = reimbTypeId;
}


public Reimbursement(int reimbId, int reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved,
		String reimbDescription, byte[] reimbReceipt, int reimbAuthor, int reimbResolver, int reimbStatusId,
		int reimbTypeId) {
	super();
	this.reimbId = reimbId;
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbResolved = reimbResolved;
	this.reimbDescription = reimbDescription;
	this.reimbReceipt = reimbReceipt;
	this.reimbAuthor = reimbAuthor;
	this.reimbResolver = reimbResolver;
	this.reimbStatusId = reimbStatusId;
	this.reimbTypeId = reimbTypeId;
}


public Reimbursement(int reimbAmount, Timestamp reimbSubmitted, String reimbDescription, int reimbAuthor,
		int reimbStatusId, int reimbTypeId) {
	super();
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbDescription = reimbDescription;
	this.reimbAuthor = reimbAuthor;
	this.reimbStatusId = reimbStatusId;
	this.reimbTypeId = reimbTypeId;
}





public Reimbursement(int reimbId, int reimbAmount, Timestamp reimbSubmitted, String reimbDescription, int reimbAuthor,
		int reimbStatusId, int reimbTypeId) {
	super();
	this.reimbId = reimbId;
	this.reimbAmount = reimbAmount;
	this.reimbSubmitted = reimbSubmitted;
	this.reimbDescription = reimbDescription;
	this.reimbAuthor = reimbAuthor;
	this.reimbStatusId = reimbStatusId;
	this.reimbTypeId = reimbTypeId;
}


/**
 * @return the reimbAmount
 */
public int getReimbAmount() {
	return reimbAmount;
}


/**
 * @param reimbAmount the reimbAmount to set
 */
public void setReimbAmount(int reimbAmount) {
	this.reimbAmount = reimbAmount;
}


/**
 * @return the reimbSubmitted
 */
public java.sql.Timestamp getReimbSubmitted() {
	return reimbSubmitted;
}


/**
 * @param reimbSubmitted the reimbSubmitted to set
 */
public void setReimbSubmitted(java.sql.Timestamp reimbSubmitted) {
	this.reimbSubmitted = reimbSubmitted;
}


/**
 * @return the reimbResolved
 */
public java.sql.Timestamp getReimbResolved() {
	return reimbResolved;
}


/**
 * @param reimbResolved the reimbResolved to set
 */
public void setReimbResolved(java.sql.Timestamp reimbResolved) {
	this.reimbResolved = reimbResolved;
}


/**
 * @return the reimbDescription
 */
public String getReimbDescription() {
	return reimbDescription;
}


/**
 * @param reimbDescription the reimbDescription to set
 */
public void setReimbDescription(String reimbDescription) {
	this.reimbDescription = reimbDescription;
}


/**
 * @return the reimbReceipt
 */
public byte[] getReimbReceipt() {
	return reimbReceipt;
}


/**
 * @param reimbReceipt the reimbReceipt to set
 */
public void setReimbReceipt(byte[] reimbReceipt) {
	this.reimbReceipt = reimbReceipt;
}


/**
 * @return the reimbAuthor
 */
public int getReimbAuthor() {
	return reimbAuthor;
}


/**
 * @param reimbAuthor the reimbAuthor to set
 */
public void setReimbAuthor(int reimbAuthor) {
	this.reimbAuthor = reimbAuthor;
}


/**
 * @return the reimbResolver
 */
public int getReimbResolver() {
	return reimbResolver;
}


/**
 * @param reimbResolver the reimbResolver to set
 */
public void setReimbResolver(int reimbResolver) {
	this.reimbResolver = reimbResolver;
}


/**
 * @return the reimbStatusId
 */
public int getReimbStatusId() {
	return reimbStatusId;
}


/**
 * @param reimbStatusId the reimbStatusId to set
 */
public void setReimbStatusId(int reimbStatusId) {
	this.reimbStatusId = reimbStatusId;
}


/**
 * @return the reimbTypeId
 */
public int getReimbTypeId() {
	return reimbTypeId;
}


/**
 * @param reimbTypeId the reimbTypeId to set
 */
public void setReimbTypeId(int reimbTypeId) {
	this.reimbTypeId = reimbTypeId;
}


/**
 * @return the reimbId
 */
public int getReimbId() {
	return reimbId;
}


@Override
public String toString() {
	return "Reimbursement [reimbId=" + reimbId + ", reimbAmount=" + reimbAmount + ", reimbSubmitted=" + reimbSubmitted
			+ ", reimbResolved=" + reimbResolved + ", reimbDescription=" + reimbDescription + ", reimbReceipt="
			+ Arrays.toString(reimbReceipt) + ", reimbAuthorUser=" + reimbAuthorUser + ", reimbResolverUser=" + reimbResolverUser + ", reimbStatus=" + reimbStatus + ", reimbType=" + reimbType 
			+ "]\n";
}


}
