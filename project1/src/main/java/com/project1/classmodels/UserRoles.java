package com.project1.classmodels;

public class UserRoles {
	
	private int userRoleId;
	private String userRole;
	
	public UserRoles() {
		// TODO Auto-generated constructor stub
	}

	public UserRoles(int userRoleId, String userRole) {
		super();
		this.userRoleId = userRoleId;
		this.userRole = userRole;
	}

	
	public int getUserRoleId() {
		return userRoleId;
	}


	
	public String getUserRole() {
		return userRole;
	}

	@Override
	public String toString() {
		return "UserRoles [userRole=" + userRole + "]";
	}
	
	
}
