package com.project1.classmodels;

public class ReimbursementType {

	private int reimbTypeId;
	private String reimbType;
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}
	public ReimbursementType(int reimbTypeId, String reimbType) {
		super();
		this.reimbTypeId = reimbTypeId;
		this.reimbType = reimbType;
	}
	/**
	 * @return the reimbTypeId
	 */
	public int getReimbTypeId() {
		return reimbTypeId;
	}
	/**
	 * @return the reimbType
	 */
	public String getReimbType() {
		return reimbType;
	}
	@Override
	public String toString() {
		return "ReimbursementType [reimbTypeId=" + reimbTypeId + ", reimbType=" + reimbType + "]";
	}
	
	
}
