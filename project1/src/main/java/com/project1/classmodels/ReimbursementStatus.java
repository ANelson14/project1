package com.project1.classmodels;

public class ReimbursementStatus {

	private int reimbStatusId;
	private String reimbStatus;
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}
	public ReimbursementStatus(int reimbStatusId, String reimbStatus) {
		super();
		this.reimbStatusId = reimbStatusId;
		this.reimbStatus = reimbStatus;
	}
	/**
	 * @return the reimbStatusId
	 */
	public int getReimbStatusId() {
		return reimbStatusId;
	}
	/**
	 * @return the reimbStatus
	 */
	public String getReimbStatus() {
		return reimbStatus;
	}
	@Override
	public String toString() {
		return "ReimbursementStatus [reimbStatusId=" + reimbStatusId + ", reimbStatus=" + reimbStatus + "]";
	}
	
}
