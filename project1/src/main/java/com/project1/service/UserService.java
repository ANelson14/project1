package com.project1.service;

import com.project1.classmodels.User;
import com.project1.dao.UserDao;
import com.project1.exceptions.FailedUpdateException;
import com.project1.exceptions.IllegalStatusException;
import com.project1.exceptions.InvalidTypeException;
import com.project1.exceptions.NoUserException;

public class UserService {

	private UserDao udao;
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	public UserService(UserDao udao) {
		super();
		this.udao = udao;
	}
	public User getUser(String name) throws NoUserException
	{
		User user = udao.selectByName(name);
		if(user == null)
		{
			throw new NoUserException();
		}
		return user;
	}
	public User getUserbyId(int id) throws NoUserException // NEEDS TEST
	{
		User user = udao.selectById(id);
		if(user == null)
		{
			throw new NoUserException();
		}
		return user;
	}
	public String getReimbursementType(int type) throws InvalidTypeException 
	{
		String type1 = udao.selectReimbType(type);
		if(type1 == null)
		{
			throw new InvalidTypeException();
		}
		return type1;
	}
	public String getReimbursementStatus(int status) throws IllegalStatusException 
	{
		String status1 = udao.selectReimbStatus(status);
		if(status1 == null)
		{
			throw new IllegalStatusException();
		}
		return status1;
	}
	public boolean userVerify(String name, String password) throws NoUserException
	{
		boolean isVerified = false;
		User user = getUser(name);
		if(user.getPassword().equals(password))
		{
			isVerified = true;
		}
		return isVerified;
	}
	public boolean updateUser(User user) throws FailedUpdateException, NoUserException
	{
		boolean isUpdated = false;
		int x;
		if(user == null)
		{
			throw new NoUserException();
		}
		x = udao.update(user);
		
		if(x ==1)
		{
			isUpdated = true;
		}
		else
		{
			throw new FailedUpdateException();
		}
		return isUpdated;
	}
	public boolean createUser(User user)
	{
		
		int x = udao.insert(user);
		if(x ==1)
		{
			return true;
		}
		return false;
	}
}
