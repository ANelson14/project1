package com.project1.service;

import java.util.ArrayList;
import java.util.List;

import com.project1.classmodels.Reimbursement;
import com.project1.dao.ReimbursementDao;
import com.project1.exceptions.FailedUpdateException;

public class ReimbursementService {

	private ReimbursementDao rdao;

	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementDao rdao) {
		super();
		this.rdao = rdao;
	}

	public List<Reimbursement> getAllEmployee(int id) {

		List<Reimbursement> rList = rdao.selectAll(id);
		// System.out.println(rList);
		if (rList.size() == 0) {
//			throw new NullPointerException();
		}
		return rList;

	}

	public List<Reimbursement> getAllManager() {
		List<Reimbursement> rList = rdao.selectEverything();
		// System.out.println(rList);
		if (rList.size() == 0) {
			throw new NullPointerException();
		}
		return rList;

	}

	public boolean update(int reimbId, int resolvid, int status) throws NoReimbException, FailedUpdateException {

		boolean isUpdated = false;
		int x = rdao.update(reimbId, resolvid, status);
		if (x == 1) {
			isUpdated = true;
			return isUpdated;
		}
		if(x == -5)
		{
		
		}
		else {
			throw new FailedUpdateException();
		}
		return isUpdated;

	}

	public boolean insert(Reimbursement user) {
		boolean isInserted = false;
		int x = rdao.insert(user);
		if (x == 0) {
			throw new NullPointerException();
		} else {
			isInserted = true;
		}
		return isInserted;
	}

}
