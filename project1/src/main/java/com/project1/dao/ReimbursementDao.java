package com.project1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.project1.classmodels.Reimbursement;

public class ReimbursementDao implements GenericDao<Reimbursement> {
	private Project1DaoConnection p1dc;
	private Reimbursement reimb;
	

	public ReimbursementDao() {
		p1dc = new Project1DaoConnection();
	}

	public ReimbursementDao(Project1DaoConnection p1dc) {
		super();
		this.p1dc = p1dc;
	}

	@Override
	public int insert(Reimbursement entity) {
		int rowsChanged = -1;
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "Insert into ers_reimbursement  (reimb_amount, reimb_submitted,reimb_description,reimb_author, reimb_status_id, reimb_type_id) values (?,?,?,?,1,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			Timestamp t = new Timestamp(System.currentTimeMillis());
			ps.setInt(1, entity.getReimbAmount());
			ps.setTimestamp(2, t);
			ps.setString(3, entity.getReimbDescription());
			ps.setInt(4, entity.getReimbAuthor());
//			ps.setInt(5, entity.getReimbStatusId());
			ps.setInt(5, entity.getReimbTypeId());

			rowsChanged = ps.executeUpdate();
			// System.out.println(rowsChanged);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rowsChanged;
	}
	public List<Reimbursement> selectEverything() {//needs a test
		List<Reimbursement> rList = new ArrayList<>();
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select reimb_id, reimb_amount, reimb_submitted,reimb_resolved, reimb_description, eu.ers_username as submitter, eu2.ers_username as resolver, ers2.reimb_status, reimb_type from ers_reimbursement ers inner join ers_reimbursement_status ers2 on ers.reimb_status_id =ers2.reimb_status_id inner join ers_users eu on eu.ers_users_id = ers.reimb_author inner join ers_reimbursement_type ert on ert.reimb_type_id = ers.reimb_type_id inner join ers_users eu2 on eu2.ers_users_id = ers.reimb_resolver ";

			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}
	public List<Reimbursement> selectAll(int id) {//needs test
		List<Reimbursement> rList = new ArrayList<>();
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select reimb_id, reimb_amount, reimb_submitted,reimb_resolved, reimb_description, eu.ers_username as submitter, eu2.ers_username as resolver, ers2.reimb_status, reimb_type from ers_reimbursement ers inner join ers_reimbursement_status ers2 on ers.reimb_status_id =ers2.reimb_status_id inner join ers_users eu on eu.ers_users_id = ers.reimb_author inner join ers_reimbursement_type ert on ert.reimb_type_id = ers.reimb_type_id inner join ers_users eu2 on eu2.ers_users_id = ers.reimb_resolver where reimb_author = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}

	@Override
	public Reimbursement selectById(int id) {
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select reimb_id, reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id from ers_reimbursement where reimb_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return null;
			}
			reimb = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4),
					rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimb;
	}

	/*
	 * public List<Reimbursement> selectByStatus(int status, int id) {
	 * List<Reimbursement> rList = new ArrayList<>(); try (Connection con =
	 * p1dc.getDBConnection()) { String sql =
	 * "select reimb_id, reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id from ers_reimbursement where reimb_status_id = ? and reimb_author=?;"
	 * ; PreparedStatement ps = con.prepareStatement(sql); ps.setInt(1, status);
	 * ps.setInt(2, id); ResultSet rs = ps.executeQuery();
	 * 
	 * while (rs.next()) { rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2),
	 * rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getInt(6),
	 * rs.getInt(7), rs.getInt(8), rs.getInt(9)));
	 * 
	 * } } catch (SQLException e) { e.printStackTrace(); } return rList; }
	 * 
	 * public List<Reimbursement> selectByType(int type, int id) {
	 * List<Reimbursement> rList = new ArrayList<>(); try (Connection con =
	 * p1dc.getDBConnection()) { String sql =
	 * "select reimb_id, reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id from ers_reimbursement where reimb_type_id = ? and reimb_author=?;"
	 * ; PreparedStatement ps = con.prepareStatement(sql); ps.setInt(1, type);
	 * ps.setInt(2, id); ResultSet rs = ps.executeQuery();
	 * 
	 * while (rs.next()) { rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2),
	 * rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getInt(6),
	 * rs.getInt(7), rs.getInt(8), rs.getInt(9)));
	 * 
	 * } } catch (SQLException e) { e.printStackTrace(); } return rList; }
	 */
	
	public int update(int reimbId, int resolver, int status) {//needs test
		int rowsChanged = -1;
		try (Connection con = p1dc.getDBConnection()) {
			String sql;
			Reimbursement r = selectById(reimbId);
			if(r.getReimbAuthor() == resolver)
			{
				return -5;
			}
			Timestamp t = new Timestamp(System.currentTimeMillis());
			sql = "update ers_reimbursement set reimb_resolved=?,reimb_resolver=?,reimb_status_id=? where reimb_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);

			
				ps.setTimestamp(1, t);
		
				ps.setInt(2, resolver);
				ps.setInt(3, status);
				ps.setInt(4, reimbId);
			

			rowsChanged = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rowsChanged;
	}

}
