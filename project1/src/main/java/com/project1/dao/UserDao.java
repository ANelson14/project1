package com.project1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project1.classmodels.User;

public class UserDao implements GenericDao<User> {
	private Project1DaoConnection p1dc;

	public UserDao() {
		p1dc = new Project1DaoConnection();
	}

	public UserDao(Project1DaoConnection p1dc) {
		super();
		this.p1dc = p1dc;
	}

	@Override
	public int insert(User entity) {
		int rowsChanged = -1;
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "Insert into ers_users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) values (?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, entity.getUsername());
			ps.setString(2, entity.getPassword());
			ps.setString(3, entity.getFirstName());
			ps.setString(4, entity.getLastName());
			ps.setString(5, entity.getEmail());
			ps.setInt(6, entity.getUserRoleId());

			rowsChanged = ps.executeUpdate();
			// System.out.println(rowsChanged);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rowsChanged;

	}

	/*
	 * public List<User> selectAll() { List<User> uList = new ArrayList<>(); try
	 * (Connection con = p1dc.getDBConnection()) { String sql =
	 * "select * from ers_users;"; PreparedStatement ps = con.prepareStatement(sql);
	 * ResultSet rs = ps.executeQuery();
	 * 
	 * while (rs.next()) { uList.add(new User(rs.getInt(1), rs.getString(2),
	 * rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),
	 * rs.getInt(7)));
	 * 
	 * } } catch (SQLException e) { e.printStackTrace(); } return uList; }
	 */
	public User selectByName(String name) {
		User user = null;
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select * from ers_users where ers_username = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,name);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return null;
			}
			user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getInt(7));
			// System.out.println(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	@Override
	public User selectById(int id) {
		User user = null;
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select * from ers_users where ers_users_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return null;
			}
			user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
					rs.getString(6), rs.getInt(7));
			// System.out.println(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}


	
	public int update(User entity) {//needs test
		User user = null;
		int rowsChanged = -1;
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "update ers_users set ers_username=?, ers_password = ?, user_last_name = ?, user_first_name=?, user_email=? where ers_users_id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, entity.getUsername());
			ps.setString(2, entity.getPassword());
			ps.setString(3, entity.getFirstName());
			ps.setString(4, entity.getLastName());
			ps.setString(5, entity.getEmail());
			ps.setInt(6, entity.getUserId());
			rowsChanged = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rowsChanged;
	}


	public String selectReimbType(int type) {
		String t1 = "";
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select * from ers_reimbursement_type where reimb_type_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,type);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return null;
			}
			t1 = rs.getString(2);
			// System.out.println(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return t1;

	}

	public String selectReimbStatus(int status) {
		String s1 = "";
		try (Connection con = p1dc.getDBConnection()) {
			String sql = "select * from ers_reimbursement_status where reimb_status_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,status);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return null;
			}
			s1 = rs.getString(2);
			// System.out.println(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s1;

	}

}
