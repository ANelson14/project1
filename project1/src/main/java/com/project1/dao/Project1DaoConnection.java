package com.project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Project1DaoConnection {
	
	private static String url = "jdbc:mariadb://database-1.cv3vopt2udbb.us-east-2.rds.amazonaws.com:3306/project1db";
	private static String user = "project1user";
	private static String password = "mypassword";
	
	public Project1DaoConnection() {
		// TODO Auto-generated constructor stub
	}
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}

}
