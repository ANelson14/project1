package com.project1.dao;

import java.util.List;

public interface GenericDao <E> {

	public int insert(E entity);
	public E selectById(int id);
	//public int update(E entity);
}
