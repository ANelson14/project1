package com.project1.dao;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import com.project1.classmodels.Reimbursement;
import com.project1.classmodels.User;
import com.util.CONSTANTVALUES;

public class DAOIMPL {

public static void main(String[] args) {
	User user;
	Project1DaoConnection p1dao = new Project1DaoConnection();
	UserDao udao = new UserDao(p1dao);		
	user = udao.selectById(3);
//	System.out.println(user);
	User user2 = new User("white_devil", "youWillBeBefriended", "Nanoha", "Takamachi", "nanoha.takamachi@midchild.com", 2); // creating a new manager
	User user3 =  new User("dragon_summoner", "AlzantzRules", "Caro", "Lu Rushe", "caro.lurushe@midchild.com",1); //creating a new employee
 	User user4 = new User("numbers_boss", "AlzantzRules", "Jail", "Scaghlietti", "jail.scaglietti@midchild.com",1);
 	
//	udao.insert(user2);
//	udao.insert(user3);
// 	udao.insert(user4);
//	List<User> uList = udao.selectAll();
//	System.out.println(uList);
//	user = udao.selectById(4);
//	System.out.println(user);
	Timestamp t = new Timestamp(System.currentTimeMillis());
	
	ReimbursementDao rdao = new ReimbursementDao(p1dao);
	Reimbursement r1 = new Reimbursement(100, t,"Test1",4, CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.TYPETRAVEL);
	Reimbursement r2 = new Reimbursement(110, t,"Test2",4, CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.TYPEMEALS);
	Reimbursement r3 = new Reimbursement(120, t,"Test3",4, CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.TYPEOTHER);
	Reimbursement r4 = new Reimbursement(130, t,"Test4",4, CONSTANTVALUES.STATUSDENIED, CONSTANTVALUES.TYPELODGINGS);
	Reimbursement r5 = new Reimbursement(140, t,"Test5",4, CONSTANTVALUES.STATUSAPPROV, CONSTANTVALUES.TYPECERTIF);
	Reimbursement r6 = new Reimbursement(150, t,"Test6",4, CONSTANTVALUES.STATUSAPPROV, CONSTANTVALUES.TYPETRAVEL);
	Reimbursement r7 = new Reimbursement(160, t,"Test7",4, CONSTANTVALUES.STATUSDENIED, CONSTANTVALUES.TYPETRAVEL);
//	System.out.println(r1);
//	rdao.insert(r1);
//	rdao.insert(r2);
//	rdao.insert(r3);
//	rdao.insert(r4);
//	rdao.insert(r5);
//	rdao.insert(r6);
//	rdao.insert(r7);
//	
	

	//System.out.println(r7);
	
	//List<Reimbursement> rList = rdao.selectAll(4);
	//System.out.println(rlist2);
//	
//	Reimbursement r2 = rdao.selectById(3);
//	System.out.println(r2);
////	
//	Reimbursement r3 = new Reimbursement(19000, t,"food for Vivio",4, CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.TYPEMEALS);
//	rdao.insert(r3);
////	System.out.println(r3);
//	Reimbursement r4 =  new Reimbursement(9,300, t,"food for Vivio",4, CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.TYPEMEALS);
//	rdao.update(r4);
//	r4 = rdao.selectById(9);
//	System.out.println(r4);
//	List<Reimbursement> reimb;
//	
//	
//	reimb = rdao.selectByStatus(CONSTANTVALUES.STATUSPEND, 5);
//	System.out.println(reimb);
//	
//	
//	
//	reimb = rdao.selectByType(CONSTANTVALUES.TYPEOTHER, 3);
////	System.out.println(reimb);
//	
//	
//	User user6 = new User(6,"general_of_raging_flame", "bookOfDarkness1", "Signum", "Needes", "signum.needes@midchild.com", 1);
////	udao.insert(user6);
////	System.out.println(user6);
//	User user7 = new User(14,"general_of_raging_flame", "bookOfDarkness1", "Signum", "Yagami", "signum.yagami@midchild.com", 1);
////	udao.update(user7);
//	
//	user = udao.selectById(14);
////	System.out.println(user);
//	
//	User user8 = udao.selectByUsername("white_devil");
////	System.out.println(user8);
	
	Reimbursement r = rdao.selectById(1067);
	System.out.println(r);
	new Reimbursement(1067,160, Timestamp.valueOf("2021-01-05 22:38:05.0"),null, "Test7",4,0,3, 2);
	
}


}
