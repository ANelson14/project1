package com.project1.controllers;

import java.util.List;

import com.project1.MainDriver;
import com.project1.classmodels.Reimbursement;
import com.project1.classmodels.User;
import com.project1.exceptions.BadRoleIdException;
import com.project1.service.ReimbursementService;
import com.project1.service.UserService;

import io.javalin.http.Handler;

public class UserController {

	private UserService uservice;
	private ReimbursementService rservice;
	public Handler postLogin = (ctx) -> {
//		System.out.println(ctx.body());
		if (uservice.userVerify(ctx.formParam("username"), ctx.formParam("password"))) {
			User user = uservice.getUser(ctx.formParam("username"));
//			System.out.println(user.getUsername());
//			System.out.println("login is verified");
			if (user.getUserRoleId() == 1) {
//				System.out.println("inside employee check");
				ctx.sessionAttribute("user", uservice.getUser(ctx.formParam("username")));
				MainDriver.log.info("successfull employee login :" + user.getUsername());
				ctx.sessionAttribute("reimb", rservice.getAllEmployee(user.getUserId()));

				ctx.redirect("/html/employeehub.html");
			} else if (user.getUserRoleId() == 2) {
				// System.out.println(user);
//				System.out.println("inside manager check");
				MainDriver.log.info("successfull employee login :"+ user.getUsername());
				ctx.sessionAttribute("user", uservice.getUser(ctx.formParam("username")));
				ctx.sessionAttribute("reimb", rservice.getAllManager());

				ctx.redirect("/html/managerhub.html");
			} else {
				System.out.println("Bad Role");
				throw new BadRoleIdException();
			}
		} else {
			ctx.redirect("/html/badlogin.html");
			System.out.println("incorrect login");
		}
	};
	public Handler getSessionUser = (ctx) -> {
		// System.out.println((User)ctx.sessionAttribute("user"));
		User user = (User) ctx.sessionAttribute("user");
//		System.out.println(user);
//		System.out.println(reimb);
		ctx.json(user);
		// System.out.println(rlist);
	};
	public Handler getEmployeeReim = (ctx) -> {
		User user = (User) ctx.sessionAttribute("user");
		List<Reimbursement> empRlist = rservice.getAllEmployee(user.getUserId());
		ctx.json(empRlist);
	};
	public Handler getreimbManSession = (ctx) -> {
		List<Reimbursement> rlist = rservice.getAllManager();
		List<Reimbursement> reimb = (List<Reimbursement>) ctx.sessionAttribute("reimb");

		ctx.json(reimb);
		
		// System.out.println((List<Reimbursement>)ctx.sessionAttribute("reimb"));
	};
	public Handler updateReimb = (ctx) -> {
		
		//System.out.println(ctx.body());
		String c = ctx.body();
		String[] a = c.split(",");
		int[] s = new int[3];
		for(int i =0;i < a.length;i++)
		{		
			s[i] = Integer.parseInt(a[i]);
			System.out.println(s[i]);
		}
		System.out.println(s[2]);
		if(s[2] != 0)
		{
			boolean r = rservice.update(s[2], s[0], s[1]);
			
			System.out.println(r);
			if(r == false)
			{
				System.out.println("unable to update own request");
			}
			
		}
		
		//String c = ctx.formParam("data");
		//System.out.println(c[1]);
		//System.out.println(c[2]);
		//System.out.println(c[3]);
		
		//System.out.println(c);
	};
	public Handler addReimb = (ctx) ->{
		System.out.println(ctx.body());
		String params = ctx.body();
		String[] paramArr = params.split(",");
		int p1 = Integer.parseInt(paramArr[0]);
		int p2 = Integer.parseInt(paramArr[1]);
		int p3 = Integer.parseInt(paramArr[3]);
		
		int x =0;
		if(x == 0)
		{
			Reimbursement r = new Reimbursement(p3, paramArr[2], p2, p1);
			System.out.println("amount: " + r.getReimbAmount());
			System.out.println("author: " + r.getReimbAuthor());
			System.out.println("type: " + r.getReimbTypeId());
			boolean t = rservice.insert(r);
			if(t == true)
			{
				System.out.println("success");
			}
			x++;
		}
		else
		{
			
		}
	};
	public Handler getlogout = (ctx) ->{
		User user = null;
		ctx.sessionAttribute("user", null);
		System.out.println((User)ctx.sessionAttribute("user"));
//		System.out.println(user);
//		System.out.println(reimb);
		ctx.json(user);
		// System.out.println(rlist);
	};
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	public UserController(UserService uservice, ReimbursementService rservice) {
		super();
		this.uservice = uservice;
		this.rservice = rservice;
	}

}
