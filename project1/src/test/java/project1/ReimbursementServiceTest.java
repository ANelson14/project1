package project1;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.classmodels.Reimbursement;
import com.project1.dao.ReimbursementDao;
import com.project1.exceptions.FailedUpdateException;
import com.project1.service.NoReimbException;
import com.project1.service.ReimbursementService;

public class ReimbursementServiceTest {

	@Mock
	private ReimbursementDao rdao;
	private ReimbursementService rservice = new ReimbursementService(rdao);
	private List<Reimbursement> testReimbursements = Arrays.asList(new Reimbursement(1038, 150, Timestamp.valueOf("2021-01-05 21:02:45"), "Test1", 4,1,2));
	private Reimbursement testReimbursment1;
	private Reimbursement testReimbursment;
	List<Reimbursement> rlistcomplete = Arrays.asList(new Reimbursement (1061, 100, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test1", 4, 0, 1, 2), new Reimbursement (1062, 110, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test2", 4, 0, 1, 4), new Reimbursement (1063, 120, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test3", 4, 0, 1, 5), new Reimbursement (1064, 130, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test4", 4, 0, 3, 3), new Reimbursement (1065, 140, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test5", 4, 0, 2, 1), new Reimbursement (1066, 150, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test6", 4, 0, 2, 2),new Reimbursement (1067, 160, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test7", 4, 0, 3, 2),new Reimbursement(1068,150,Timestamp.valueOf("2021-01-06 05:13:26.0"), Timestamp.valueOf("2021-01-06 05:13:26.0"),"purchased uniform for work purposed",3,0,1,5));
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		rservice = new ReimbursementService(rdao);
		testReimbursment = new Reimbursement(1061, 100, Timestamp.valueOf("2021-01-05 22:38:05"), "Test1", 4,1,2);
		 testReimbursment1 = new Reimbursement(1061, 100, Timestamp.valueOf("2021-01-05 22:38:05"), "Test1", 4,3,2);

		when(rdao.selectAll(4)).thenReturn(testReimbursements);
		when(rdao.selectEverything()).thenReturn(rlistcomplete);
		when(rdao.update(1061,4,3)).thenReturn(1);
	}
	@Test
	public void selectAllSuccess()
	{
		List<Reimbursement> rlist = rservice.getAllEmployee(4);
		//System.out.println(rlist);
		assertEquals(rservice.getAllEmployee(4).get(0).getReimbId(), testReimbursements.get(0).getReimbId());
	}
//	@Test(expected = NullPointerException.class)
//	public void selectAllFailure()
//	{
//		assertEquals(rservice.getAllEmployee(5).get(0).getReimbId(), testReimbursements.get(0).getReimbId());
//	}
	@Test
	public void selectAllManagerSuccess()
	{
		assertEquals(rservice.getAllManager(), rlistcomplete);
	}
	@Test
	public void updateSuccess() throws NoReimbException, FailedUpdateException
	{
		assertEquals(rservice.update(1061,4,3), true);
	}


}
