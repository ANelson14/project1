package project1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.project1.classmodels.Reimbursement;
import com.project1.dao.Project1DaoConnection;
import com.project1.dao.ReimbursementDao;

public class ReimbursementDaoTest {
	@Mock
	private Project1DaoConnection p1dc;
	
	private int rowsChanged;
	@Mock
	private Connection c;
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	@Mock
	
	private Reimbursement reimb;
	List<Reimbursement> rlistcomplete = Arrays.asList(new Reimbursement (1061, 100, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test1", 4, 0, 1, 2), new Reimbursement (1062, 110, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test2", 4, 0, 1, 4), new Reimbursement (1063, 120, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test3", 4, 0, 1, 5), new Reimbursement (1064, 130, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test4", 4, 0, 3, 3), new Reimbursement (1065, 140, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test5", 4, 0, 2, 1), new Reimbursement (1066, 150, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test6", 4, 0, 2, 2),new Reimbursement (1067, 160, Timestamp.valueOf("2021-01-05 22:38:05.0"), null, "Test7", 4, 0, 3, 2),new Reimbursement(1068,150,Timestamp.valueOf("2021-01-06 05:13:26.0"), Timestamp.valueOf("2021-01-06 05:13:26.0"),"purchased uniform for work purposed",3,0,1,5));
	Reimbursement reimb1;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(p1dc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		reimb = new Reimbursement(1061, 100, Timestamp.valueOf("2021-01-05 22:38:05"), "Test1", 4,1,2);
		 reimb1 = new Reimbursement(1061, 100, Timestamp.valueOf("2021-01-05 22:38:05"), "Test1", 4,3,2);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(reimb.getReimbId());
		when(rs.getInt(2)).thenReturn(reimb.getReimbAmount());
		when(rs.getTimestamp(3)).thenReturn(reimb.getReimbSubmitted());
		when(rs.getTimestamp(4)).thenReturn(reimb.getReimbResolved());
		when(rs.getString(5)).thenReturn(reimb.getReimbDescription());
		when(rs.getInt(6)).thenReturn(reimb.getReimbAuthor());
		when(rs.getInt(7)).thenReturn(reimb.getReimbResolver());
		when(rs.getInt(8)).thenReturn(reimb.getReimbStatusId());
		when(rs.getInt(9)).thenReturn(reimb.getReimbTypeId());
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.executeUpdate()).thenReturn(1);
		
	}
	
	@Test
	public void selectByReimbIdSuccess()
	{
		//System.out.println(reimb.getReimbAmount());
		assertEquals(new ReimbursementDao(p1dc).selectById(1061).getReimbAmount(), reimb.getReimbAmount());
	}
//	@Test
//	public void selectAllReimbursementsByEmployeeIdSuccess()
//	{
//		
//	}
//	@Test
//	public void selectEverythingSuccess() throws SQLException
//	{
//		when(rs.next()).thenReturn(true).thenReturn(false);
//		assertEquals(new ReimbursementDao(p1dc).selectEverything(), rlistcomplete);
//	}
	@Test
	public void updateSuccess()
	{
		assertEquals(new ReimbursementDao(p1dc).update(1061,5,3), 1);
	}

}
