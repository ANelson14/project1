package project1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.pagemodels.UserLoginPage;

public class SeleniumTesting {

	private UserLoginPage page;
	
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String fPath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", fPath);
		
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		driver.quit();
	}
	@Before
	public void setUp() throws Exception{
		this.page = new UserLoginPage(driver);
	}
	@Test
	public void testSuccessfullLogin() {
		page.setUsername("dragon_rider_erio");
		page.setPassword("mypassword");
		page.submitLogin();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/employeehub.html"));
		assertEquals("http://localhost:9011/html/employeehub.html", driver.getCurrentUrl());		

	}
	@Test
	public void testFailedlLogin() {		
		page.setUsername("test");
		page.setPassword("t");
		page.submitLogin();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/badlogin.html"));
		System.out.println(driver.getCurrentUrl());
		assertEquals("http://localhost:9011/html/badlogin.html", driver.getCurrentUrl());		
	}
	@Test
	public void testSuccessfullCreateReimb() {
		  //UserLoginPage page = new UserLoginPage(driver); 
		  page.setUsername("dragon_rider_erio");
		  page.setPassword("mypassword"); 
		  page.submitLogin();
		  page.navigateTo2("/html/submitmanReimbursement.html");
		  page.setReimbamount("150"); page.setDescription("Selenium Testing");
		  page.setSelectNumbs("3"); page.submitReimb();
	}
	@Test
	public void testSuccessfullManagerLogin() {
		  page.setUsername("test");
		  page.setPassword("test"); 
		  page.submitLogin();
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.urlMatches("/managerhub.html"));
			assertEquals("http://localhost:9011/html/managerhub.html", driver.getCurrentUrl());	
	}
	@Test
	public void testSuccessfulManagerApprove() {
		page.setUsername("mistress_of_the_night_sky");
		page.setPassword("windOfBlessingReinforce");
		page.submitLogin();
		page.approveReimb(".//*[@id='2028']");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/managerhub.html"));
		assertEquals("http://localhost:9011/html/managerhub.html", driver.getCurrentUrl());			
		
		
	}
	@Test
	public void testFailedManagerApprove() {
		page.setUsername("test");
		page.setPassword("test");
		page.submitLogin();
		page.approveReimb(".//*[@id='3002']");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/managerhub.html"));
		assertEquals("http://localhost:9011/html/managerhub.html", driver.getCurrentUrl());	
	}
}
