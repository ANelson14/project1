package project1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.classmodels.Reimbursement;
import com.project1.classmodels.User;
import com.project1.dao.UserDao;
import com.project1.exceptions.FailedUpdateException;
import com.project1.exceptions.IllegalStatusException;
import com.project1.exceptions.InvalidTypeException;
import com.project1.exceptions.NoUserException;
import com.project1.service.UserService;
import com.util.CONSTANTVALUES;

public class UserServiceTest {

	@Mock
	private UserDao udao;
	private UserService userservice = new UserService(udao);
	private User testUser;
	private Reimbursement testReimbursement;
	private User u1 = new User(7,"numbers_boss", "imacreepydude", "Jail", "Scaghlietti","jail.scaglietti@midchild.com", 2);
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		userservice = new UserService(udao);
		testUser = new User(3, "mistress_of_the_night_sky", "windOfBlessingReinforce", "Hayate", "Yagami",
				"hayate.yagami@midchild.com", 2);
		when(udao.selectByName("mistress_of_the_night_sky")).thenReturn(testUser);
		when(udao.selectById(3)).thenReturn(testUser);
		testReimbursement = new Reimbursement(1066, 150, Timestamp.valueOf("2021-01-05 22:38:05"), "Test6", 4,3,2);
		when(udao.update(u1)).thenReturn(1);
	}

	@Test
	public void testgetUserSuccess() throws NoUserException {
		assertEquals(userservice.getUser("mistress_of_the_night_sky"), testUser);
	}

	@Test(expected = NoUserException.class)
	public void testgetUserFailure() throws NoUserException {
		assertEquals(userservice.getUser("blahblahblau"), null);
	}

	@Test
	public void testVerifyPasswordSuccess() throws NoUserException {
		assertTrue(userservice.userVerify("mistress_of_the_night_sky", "windOfBlessingReinforce" ));
	}

	@Test
	public void testVerifyPasswordFailure() throws NoUserException {
		assertFalse(userservice.userVerify("mistress_of_the_night_sky", "windOfBlessingReinforceZwei" ));
	}
	@Test
	public void testgetUserbyIdSuccess() throws NoUserException
	{
		assertEquals(userservice.getUserbyId(3), testUser);
	}
	@Test(expected = NoUserException.class)
	public void testgetUserbyIdFailure() throws NoUserException
	{
		assertEquals(userservice.getUserbyId(1), null);
	}
	@Test
	public void testgetReimbStatusSuccess() throws IllegalStatusException
	{
		when(udao.selectReimbStatus(3)).thenReturn(CONSTANTVALUES.APPROVED);
		assertEquals(userservice.getReimbursementStatus(testReimbursement.getReimbStatusId()), CONSTANTVALUES.APPROVED);
	}
	@Test(expected = IllegalStatusException.class)
	public void testgetReimbStatusFailure() throws IllegalStatusException
	{
		when(udao.selectReimbStatus(5)).thenReturn(CONSTANTVALUES.APPROVED);
		assertEquals(userservice.getReimbursementStatus(testReimbursement.getReimbStatusId()), CONSTANTVALUES.APPROVED);
	}
	@Test
	public void testgetReimbTypeSuccess() throws InvalidTypeException
	{
		when(udao.selectReimbType(2)).thenReturn(CONSTANTVALUES.TRAVEL);
		assertEquals(userservice.getReimbursementType(testReimbursement.getReimbTypeId()), CONSTANTVALUES.TRAVEL);
	}
	@Test(expected = InvalidTypeException.class)
	public void testgetReimbTypeFailure() throws InvalidTypeException
	{
		when(udao.selectReimbType(2)).thenReturn(CONSTANTVALUES.TRAVEL);
		assertEquals(userservice.getReimbursementType(6), CONSTANTVALUES.TRAVEL);
	}
	@Test
	public void updateUserSuccess() throws FailedUpdateException, NoUserException
	{
		assertEquals(userservice.updateUser(u1), true);
	}
	@Test(expected=NoUserException.class)
	public void updateUserFailure() throws FailedUpdateException, NoUserException
	{
		assertEquals(userservice.updateUser(null), true);
	}
//	@Test
//	public void testCreateUserSuccess()
//	{
//		//fail();	
//		assertTrue(userservice.createUser(new User("lightning_quick_fate", "arfIsMyFirstFriend", "Fate", "Harloawn", "fate.harloawn@midchild.com", 2)));
//	}
//	@Test
//	public void testCreateuserFailure()
//	{
//		fail();
//	}
}
