package project1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.classmodels.ReimbursementStatus;
import com.project1.classmodels.ReimbursementType;
import com.project1.classmodels.User;
import com.project1.dao.Project1DaoConnection;
import com.project1.dao.UserDao;
import com.util.CONSTANTVALUES;

public class UserDaoTest {

	@Mock
	private Project1DaoConnection p1dc;
	
	private int rowsChanged = 1;
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;

	private User testUser2;
	private User testUser1;
	private ReimbursementStatus testStatus;
	private ReimbursementType testType;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(p1dc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testUser1 = new User(4, "dragon_rider_erio", "ExCforever", "Erio", "Mondial", "erio.mondial@midchild.com", 1);
		testUser2 = new User(4, "dragon_rider_erio", "mypassword", "Erio", "Mondial", "erio.mondial@midchild.com", 1);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser2.getUserId());
		when(rs.getString(2)).thenReturn(testUser2.getUsername());
		when(rs.getString(3)).thenReturn(testUser2.getPassword());
		when(rs.getString(4)).thenReturn(testUser2.getFirstName());
		when(rs.getString(5)).thenReturn(testUser2.getLastName());
		when(rs.getString(6)).thenReturn(testUser2.getEmail());
		when(rs.getInt(7)).thenReturn(testUser2.getUserRoleId());
		
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.executeUpdate()).thenReturn(rowsChanged);
		
	}

	@Test
	public void testSuccessSelectById(){ 
		assertEquals(new UserDao(p1dc).selectById(4).getUsername(), testUser2.getUsername());
		
	}
	@Test
	public void selectBynameSuccess()
	{
		assertEquals(new UserDao(p1dc).selectByName("dragon_rider_erio").getUserId(), testUser2.getUserId());
	}
	@Test
	public void updateSuccess()
	{
		assertEquals(new UserDao(p1dc).update(testUser1), 1);
	}
	
	@Test
	public void selectTypeSuccess() throws SQLException
	{
		testType = new ReimbursementType(CONSTANTVALUES.TYPEOTHER, CONSTANTVALUES.OTHER);
		when(rs.getString(2)).thenReturn(testType.getReimbType());
		assertEquals(new UserDao(p1dc).selectReimbType(5), testType.getReimbType());
	}
	@Test
	public void selectStatusSuccess() throws SQLException
	{
		testStatus = new ReimbursementStatus(CONSTANTVALUES.STATUSPEND, CONSTANTVALUES.PENDING);
		when(rs.getString(2)).thenReturn(testStatus.getReimbStatus());
		assertEquals(new UserDao(p1dc).selectReimbStatus(CONSTANTVALUES.STATUSPEND), testStatus.getReimbStatus());
	}


}
