# ERS_REIMBURSEMENT_SYSTEM

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement. 


## Technologies Used

* Javalin - Version 3.10.1
* slf4j - Version 1.7.30 
* Log4J - Version 1.2.17
* Java - Version 1.8
* Selenium Version - 3.141.59 
* Jackson - Version -2.10.3
* Junit - Version 4.12
* Mockito - Version 1.10.19
* Mariadb - 2.6.2


## Features

* Take away functionality on being able to deny approved requests
* Able to log into an employee page and submit reimbursements
* Able to log into a manager page and submit reimbursements as an employee, also able to approve/deny reimbursements.


To-do list:
* Fix issue with table not automatically updating after submitting a reimbursement
* Fix the Back Button issue for being able to approve/deny reimbursements
* Add Functionality for creating employee users

## Getting Started
   
git clone https://gitlab.com/ANelson14/project1.git

This system will work in any browser

## Usage

Open the project in a STS style IDE log4jproperties update the log4j.appener.file.File to a file on your local machine. 
Run the MainDriver.java. go to your favorite browser and go to http://localhost:9011/html/index.html.
From there you can log into the reimbursement system as an Employee (e.g. employee username: dragon_rider_erio e.g. employee password: mypassword) or as a Financial Manager (e.g. manager username: test e.g. manager password: test)
You will be able to submit reimbursements in the submit new reimbursement page. You can approve/deny reimbursements in the managerhub.html
Logging information will be found in the project.log


